Bootstrap: docker
From: ubuntu:20.04

%labels
    Author ABiMS
    Version 1.3

%files
    /src/hectar/1.3/hectar-1.3.tar.gz /opt/

%post
    apt-get -qqqq update && apt-get -qqqq upgrade -y
    apt -y -qqqq install tar gzip locales
    apt -y -qqqq install perl
    apt -y -qqqq install libswitch-perl libmime-base64-perl libparallel-forkmanager-perl
    apt -y -qqqq install gawk libc6-i386 # respectively for signalp, phobius/decodeanhmm
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -y -qqqq install default-jre # for predisi/java

    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

    cd /opt/
    tar -zxf hectar-1.3.tar.gz
    cd hectar

    rm -rf /opt/hectar-1.3.tar.gz
    apt-get clean
    apt-get purge

%environment
    export PATH=/opt/hectar:$PATH
    export LANG=en_US.utf8
    export HECTAR_HMMTOP_ARCH=/opt/hectar/predsoft/hmmtop/hmmtop.arch
    export HECTAR_HMMTOP_PSV=/opt/hectar/predsoft/hmmtop/hmmtop.psv

%runscript
    echo "HECTAR uses DTU Health Tech SignalP and TargetP that are dedicated to Academic usage and require a license agreement: https://services.healthtech.dtu.dk/software.php"
    exec "$@"

%test
    export PATH=/opt/hectar:$PATH
    export LANG=en_US.utf8
    export HECTAR_HMMTOP_ARCH=/opt/hectar/predsoft/hmmtop/hmmtop.arch
    export HECTAR_HMMTOP_PSV=/opt/hectar/predsoft/hmmtop/hmmtop.psv
    hectar.pl --help | grep "hectar.pl -i input.fasta"
    hectar.pl -i /opt/hectar/test/test_all.fasta -d /tmp/hectar_build_test | grep "Retrieve results in getHecResults()"
